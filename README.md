Altruja
===========

Altruja online donations integration for Drupal.
This module allows you to embed an Altruja campaign page into your website.

Instructions
------------

Unpack in the *modules* folder (currently in the root of your Drupal 8
installation) and enable in `/admin/modules`.

Then, visit `/admin/config/services/altruja/settings` and enter the email that you use
for authentication with the altruja service.

Usage
----------------

This module allows you to create blocks for each Altruja donation page that you have configured in your Altruja settings on altruja.de.
To integrate a campaign page into your website, follwo these steps:
* go to /admin/config/services/altruja/blocks and create a new block
* enter a label for the block
* enter the Altruja code for your campaign page
* save the block
* place the block either using the default block management page or any other block placement logic you use


Altruja services
----------------

https://www.altruja.de
